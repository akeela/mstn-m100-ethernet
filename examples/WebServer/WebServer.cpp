/**
 * @description: Пример простого веб-сервера
 * @author's: akeela, Fla6
 */
#include "main.h"
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include "mstn.h"
#include "w5100/w5100.h"
#include "Ethernet.h"
#include "Arduino_w.h"

#include "Dhcp.h"
int main(int argc, char *argv[])
{
    Delay (4000);
    
    // Введите IP-адресс для МК (Если не используете DHCP)
    //uint8_t caddr [] = {192,168,0,155};
    
    // МАС адрес устройства
    uint8_t mac[] = { 0x90, 0xAD, 0xDA, 0x0D, 0x96, 0xFE };

    // Инициализирует сервер с портом 80 (HTTP = 80)
    EthernetServer server(80);
    
    // Запращиваем IP адрес у DHCP сервера
    Ethernet.begin(mac); 
    
    uint8_t * your_ip = Ethernet.localIP();
    char str [256];
    printf ("ip: %hu.%hu.%hu.%hu \n",your_ip[0],your_ip[1],your_ip[2],your_ip[3]);
    while (1) 
    {   
        // Прослущивание новый клиентов
        EthernetClient client = server.available();  
        if (client) 
        {   
            // До тех пор пока есть соединение
            while (client.connected()) 
            {
                snprintf(str, sizeof str, "%hu.%hu.%hu.%hu", your_ip[0], your_ip[1], your_ip[2], your_ip[3]);
                server.write("HTTP/1.1 200 OK\r\n");
                server.write("Content-Type: text/html\r\n");
                server.write("\r\n<html><h1>Hello World!</h><p>your ip is ");
                server.write(str);
                server.write("</p></html>\r\n");
                //выходим из цикла, чтобы прослушивать новых клиентов
                break;
            }
        }
        Delay(1);
        client.stop();
    }
    return EXIT_SUCCESS;
}
