# Ethernet Library для mstn-m100
С помощью **Ethernet Shield (W5100)**, данная библиотека позволяет плате **msnt-m100** работать в локальных вычислительных сетях для приёма и передачи данных в сети Интернет. Библиотека поддерживает до четырех одновременных подключений (входящими и исходящими).

## Установка библиотеки
После распаковки исходных файлов, включите данных заголовок библиотеки в свой проект
```C++
#include "Ethernet.h"
```
## Подключение к плате
Плата msnt-m100 работает с Ethernet Shield по интерфейсу **SPI**. Цифровые порты 10, 11, 12 и 13 являются портами SS, MOSI, MISO и SCK. 

![Connection](https://gitlab.com/akeela/mstn-m100-ethernet/raw/development/pictures/pinconnections.png)
